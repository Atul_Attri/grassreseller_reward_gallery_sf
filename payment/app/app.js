/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 6/10/13
 * Time: 10:12 AM
 * To change this template use File | Settings | File Templates.
 */
/// <reference path="../Scripts/angular-1.1.4.js" />

/*#######################################################################

 Rahul Guha

 Normally like to break AngularJS apps into the following folder structure
 at a minimum:

 /app
 /controllers
 /directives
 /services
 /partials
 /views
********************CHECKOUT APP
 #######################################################################*/

var app = angular.module('ECOMM.payment', ['ngRoute', 'LocalStorageModule']);
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);
//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider) {
    $routeProvider
        .when('/payment/:status',
        {
            controller: 'paymentController',
            templateUrl: '../payment/app/partials/payment.html'
        })
        .otherwise({ redirectTo: '/home' });
});
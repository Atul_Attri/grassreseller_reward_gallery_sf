/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/10/13
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */


app.controller('NbController', function ($scope, $http, $q, $location, $window,
                                         $routeParams, nbService,localStorageService, cartService ) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    function check_login (){
        if (localStorageService.get('user_info') == null){
            $window.location.href = root_url + "/index.html#/login";
        }
        else
        {
            $window.location.href = root_url + "/home.html";
        }
    }
    $scope.select_address = function(selected_address){
        populate_selected_address(selected_address);
    }
    var get_user_shipping = function(){
        shipService.get_user_shipping_list($http, $q, localStorageService.get('user_info').email_id).then(function(data){
                $scope.user_shipping_list =  data;
                //console.log($scope.user_shipping_list) ;
            },
            function(){
                //Display an error message
                $scope.error= error;
            });
    }
    $scope.save_data = function(){

        // TODO - validate data entry

        var ship_data = [];
        ship_data.push(cartService.get_Cart());
        ship_data.push(selected_address);
        shipService.save_shipping_page($http, $q , selected_address).then (function(data){
           
        });
    }
    var populate_selected_address = function (address){
        selected_address.id = address.id;
        selected_address.name = address.name;
        selected_address.shipping_name = address.shipping_name;
        selected_address.address= address.address;
        selected_address.city= address.city;
        selected_address.state = address.state;
        selected_address.pincode = address.pincode;
        selected_address.mobile_number= address.mobile_number;
        selected_address.user_id = localStorageService.get('user_info').email_id;
        selected_address.store = store_signature;
        $scope.selected_address = selected_address;
    }
    var order_id;
    function get_order_id(){

    }
    function init() {
        // get order_id
        order_id = $routeParams.order_id;
        // get selected address
        $scope.selected_address = localStorageService.get('selected_ship_address');;
        // Get bank list
        $scope.bank_list = nb_gateways;
        console.log($scope.bank_list );


//        get_user_shipping(); // populates  $scope.user_shipping_list
//
//        populate_selected_address({
//            id : 0, name : "", shipping_name : "",
//            address: "", city: "", state : "",
//            pincode : "", mobile_number : "", user_id : localStorageService.get('user_info').email_id,
//            store : store_signature
//        });
        console.log( $scope.selected_address)  ;

    }
    var selected_address = {};
    init();

    $scope.route_to = function (page){
        var url = "/" + page + "/" + order_id;
        $location.path(url);
    }

});
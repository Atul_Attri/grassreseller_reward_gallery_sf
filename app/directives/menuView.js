/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 4/7/13
 * Time: 6:41 PM
 * To change this template use File | Settings | File Templates.
 */

app.directive('menuView', function () {
    // I allow an instance of the directive to be hooked
    // into the user-interaction model outside of the
    // AngularJS context.
    function link( $scope, element, attributes ) {

        var expression = attributes.menuView;
        alert (expression);
        var duration = ( attributes.slideShowDuration || "fast" );
        if ( ! $scope.$eval( expression ) ) {
            element.hide();
        }
        $scope.$watch(

            expression,

            function( newValue, oldValue ) {

                // Ignore first-run values since we've
                // already defaulted the element state.
                if ( newValue === oldValue ) {

                    return;

                }

                // Show element.
                if ( newValue ) {

                    element
                        .stop( true, true )
                        .slideDown( duration )
                    ;

                    // Hide element.
                } else {

                    element
                        .stop( true, true )
                        .slideUp( duration )
                    ;

                }

            }
        );

    }


    return {
        link: link,
        restrict: "A"
    }
});

/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 29/6/13
 * Time: 8:16 AM
 * To change this template use File | Settings | File Templates.
 */

//var App = angular.module('zoom', []);
app.directive('scrollView', function ($timeout) {
    return {
        // Restrict it to be an attribute in this case
        restrict: 'A',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function(scope, element, attrs) {
            $timeout(function(){
                //alert ($(element).clientWidth) ;
                $(element)
                    .showArrows(true)
                    .jScrollPane();
            });
        }
    }
});

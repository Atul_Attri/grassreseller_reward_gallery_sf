/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 14/9/13
 * Time: 1:25 PM
 * To change this template use File | Settings | File Templates.
 */
app.service('myorderService', function (localStorageService, utilService) {

    
   
    this.get_order_details = function ($http, $q, user_id) {
        var apiPath = cat_service_url + '/store/order_details?email=' + user_id ;
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }
});
﻿app.controller('loginController', function ($scope, $http, $q, $window, $location, $rootScope, loginService,
                                           state_managerService, localStorageService, cartService) {


    $scope.$on("login_success", function () {
        // This function listens to any change in scope for broadcast message login_success.
        // If it gets that, applies subsequent code
        set_login_text();




    });
    var set_login_text = function () {
        //        console.log(localStorageService.get('user_info').first_name == null )   ;
        if ((localStorageService.get('user_info') != null)) {
            if (localStorageService.get('user_info').first_name != null) {
                user_info = localStorageService.get('user_info');
                $scope.signin_text = ("welcome " + user_info.first_name + " " + user_info.last_name).toTitleCase();
                $scope.signout_text = "Sign Out";
            }
            else {
                var login_url = root_url + "/login/login.html#/login";
                $window.location.href = login_url;
            }

        }
        else {
            //            console.log("open login");
            var login_url = root_url + "/login/login.html#/login";
            $window.location.href = login_url;
        }
    }

    init();
    //    function check_login (){
    //        if (localStorageService.get('user_info') == null){
    //            //$window.location.href = root_url + "/index.html#/login";
    //            $window.location.href = root_url + "/login/login.html#/login";
    //        }
    //    }

    function init() {
        //check_login();
        var user_info;
        set_login_text();
    }

    $scope.opts = {
        backdropFade: true,
        dialogFade: true
    };

    $scope.openRegistration = function () {
        localStorageService.set('user_info', "");
        localStorageService.set('local_cart', "[]");
        localStorageService.set('local_wishlist', "[]");
        
        state_managerService.refer_url = $location.path();
        var url = root_url + "/login/login.html#/registration";
        console.log(url);
        $window.location.href = url;
    }
    $scope.openLogin = function () {

        localStorageService.set('user_info', "[]");
        localStorageService.set('local_cart', "");
        localStorageService.set('local_wishlist', "");
        
        state_managerService.refer_url = $location.path();
        var login_url = root_url + "/login/login.html#/login";
        $window.location.href = login_url;
    }
    $scope.execLogout = function () {
        var url;
        $window.location.href = login_url;
        if (localStorageService.get('user_info') != null) {
            localStorageService.add('user_info', "");
            this.openLogin();
        }
        else {
            localStorageService.add('user_info', "");
            this.openRegistration();
            //            url =  root_url + "/index.html#/registration" ;
            //            $window.location.href = url;

        }
    }

    $scope.validateUserService = function () {

        //console.log($scope.userlogin);
        //console.log(state_managerService.refer_url);
        var returnval = '';
        loginService.IsValidUser($http, $q, $scope.userlogin).then(function (data) {
            //Update UI using data or use the data to call another service
            //$scope.message = data;
            alert(data[0].message);
            console.log(data);
            if (data[0].message == "Log In Successfully") {
                // now get latest cart
                loginService.get_cart_by_email($http, $q, localStorageService.get('user_info').email_id).then(function (data) {
                    //Update UI using data or use the data to call another service
                    cartService.cart = data;
                    //get_wishlist_by_email();
                    localStorageService.add('local_cart', data);
                    console.log(cartService.cart);
                    console.log(localStorageService.get('local_cart'));

                    $rootScope.$broadcast('cart_loaded');

                    $scope.message = "Login Success";
                    $scope.user_name = $scope.userlogin.email_id;
                    $rootScope.$broadcast('login_success');
                    if (state_managerService.refer_url == null) {
                        //state_managerService.refer_url = root_url + "/index.html#/home";
                        state_managerService.refer_url = root_url + "/index.html#/cat/2.7.1";
                    }
                    state_managerService.refer_url = root_url + "/index.html#/cat/2.7.1";
                    $location.path(state_managerService.refer_url);
                    $scope.isVisible = true;

                },
                    function () {
                        //Display an error message
                        $scope.error = error;
                    });


            }
            else {
                //$scope.message = data.value;
                $scope.message = data[0].message;
                $scope.signin_text = "Sign In"
                localStorageService.set('user_info', "");
            }

            localStorageService.set('user_info', data[0]);

        },
            function () {
                //Display an error message
                $scope.error = error;
            });


    };


   


    //var get_wishlist_by_email = function () {
       
    //    alert('Ho');

    //    loginService.get_wishlist_by_email($http, $q, localStorageService.get('user_info').email_id).then(function (data) {

    //        wishlistService.wishlist = data;
    //        localStorageService.add('local_wishlist', data);
    //        console.log(wishlistService.cart);
    //        console.log(localStorageService.get('local_wishlist'));

    //        $rootScope.$broadcast('wishlist_loaded');
    //    },
    //    function () {
    //        //Display an error message
    //        $scope.error = error;
    //    });



    //}


});
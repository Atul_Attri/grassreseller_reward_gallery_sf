/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 17/9/13
 * Time: 1:20 PM
 * To change this template use File | Settings | File Templates.
 */

app.controller('specialController', function (
    $scope, $rootScope, $timeout,
    $location, $routeParams, $http,
    $q, specialService, utilService,nextService,
    cartService, state_managerService, localStorageService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    var cat_id = $routeParams.catId;
    init();
    $scope.sort_defn = "Sort By ..."
    $scope.$watch('sortorder', function () {
        switch ($scope.sortorder) {
            case "Name":
                $scope.sort_defn = "Name";
                break;
            case "+price.final_offer":
                $scope.sort_defn = "Price (Low to High)";
                break;
            case "-price.final_offer":
                $scope.sort_defn = "Price (High to Low)";
                break;
            case "-price.discount":
                $scope.sort_defn = "Overall Discount (high to low)";
                break;
            case "+brand":
                $scope.sort_defn = "Brand";
                break;
            default:
                $scope.sort_defn = "Sort By ...";
                break;
        }
    })

    function set_filter(cat_id) {
        // get data
        //alert (cat_id);
        var cat_details, selected_cat;// = catService.get_cat_details(cat_id, $http, $q);
        specialService.get_cat($http, $q, cat_id).then(function (data) {
            //Update UI using data or use the data to call another service
            cat_details = data;
            selected_cat = JSON.parse(cat_details[0].toString());
            $scope.cat_name = selected_cat.Name;
            //$scope.filters = selected_cat.filters;
            $scope.checked_vals = [];
        },
            function () {
                //Display an error message
                $scope.error = error;
            });
        specialService.get_special_filters_specific($http, $q, cat_id).then(function (data) {
            //Update UI using data or use the data to call another service
            special_filters = data;
            $scope.filters = special_filters;
            //console.log(special_filters );
        },
            function () {
                //Display an error message
                $scope.error = error;
            });

        specialService.get_cat_details($http, $q, cat_id).then(function (data) {
            //Update UI using data or use the data to call another service
            if (JSON.parse(data[0].toString()).image_urls.length > 0) {
                $scope.cat_image = JSON.parse(data[0].toString()).image_urls[0].link;
            }
            else {
                $scope.cat_image = "";
            }
        },
            function () {
                //Display an error message
                $scope.error = error;
            });


        specialService.get_breadcrumb($http, $q, cat_id).then(function (data) {
            //Update UI using data or use the data to call another service
            cats = data;
            $scope.cats = [];
            // hardcoded home node
            var home = '{"id": "0","Name": "Home","description": "Go to Home"}';
            $scope.cats.push(JSON.parse(home));
            for (i = 0; i < cats.length; i++) {
                $scope.cats.push(JSON.parse(cats[i].toString()));
            }
        },
            function () {
                //Display an error message
                $scope.error = error;
            });
        $scope.checked_vals = ""


        //        $scope.$watch('checked_vals', function(v){
        //            console.log('changed', v);
        //        });
    }

    function check_login() {
        if (localStorageService.get('user_info') == null) {
            $location.path("/login");
        }
    }

    var brand_exclusion = utilService.get_brand_exclusion();


    function init() {
        check_login();
        var lastChar = cat_id[cat_id.length - 1]; // => "1"
        //console.log (cat_id.split(".").length)     ;
        if ((lastChar == "0") && (cat_id.split(".").length == 2)) { // top level menu
            $location.path("/cat1/" + cat_id);
        }
        $scope.cat_id = cat_id;
        $scope.sortorder = 'Name';
        set_filter(cat_id);
        var prod_list;
        user_info = localStorageService.get('user_info');
        specialService.get_prod_by_cat($http, $q, cat_id, user_info.min_point_range, user_info.max_point_range).then(function (data) {
            //Update UI using data or use the data to call another service
            prod_list = data;
            process_price_logic(prod_list);
            get_brands();
            //console.log($scope.brands_filter)  ;
        },
            function () {
                //Display an error message
                $scope.error = error;
            });
        // search brands within searched prod list

    }
    var process_price_logic = function (prod_list) {
        var productList = [];
        var conv_ratio = utilService.get_conversion_ratio();
        var store_type = utilService.get_store_type();
        var customer_discount = utilService.get_customer_discount();
        var cat_discount = utilService.get_cat_discount();
        var sku_discount = utilService.get_sku_discount();

        var brand_discount = utilService.get_brand_discount();

        var arr_cat, arr_sku, special_price;
        special_price = 0;

        if (store_type != "R") // store is not Retail Only
        {
            for (i = 0; i < prod_list.length; i++) {
                prod = JSON.parse(prod_list[i].toString());
                if ((typeof prod.price) == "object") {
                    prod_price = prod.price;
                }
                else {
                    prod_price = JSON.parse(prod.price.toString());
                }

                if (customer_discount != "") {
                    prod_price.final_offer = prod_price.final_offer * (1 - parseFloat(customer_discount).toFixed(2));
                    special_price = 1;
                    //console.log(prod.price.final_offer);
                }
                arr_cat = prod.cat_id; // cats this product belongs to
                //for (k = 0; k < cat_discount.length; k++) // for each of deal cats
                //{
                //    if (arr_cat.indexOf(cat_discount[k].id) > -1) {
                //        prod_price.final_offer = prod_price.final_offer * (1 - parseFloat(cat_discount[k].extra_off).toFixed(2));
                //        special_price = 1;
                //    }
                //}
                //for (k = 0; k < sku_discount.length; k++) // for each of deal cats
                //{
                //    if (prod.sku == sku_discount[k].id) {
                //        //Diwakar 12/2/2014 discount sku was not getting calculated, hence the change. Note: cat has the same 
                //        //problem need to be fixed. Currently its not used.
                //        //Start//
                //        prod_price.final_discount = ((parseFloat(prod_price.final_discount.toFixed(2)) + (parseFloat(sku_discount[k].extra_off).toFixed(2) * 100))) / 100;
                //        prod_price.final_offer = parseInt(prod_price.mrp.toFixed(0)) * (1 - parseFloat(prod_price.final_discount).toFixed(2));
                //        //end
                //        //console.log(prod_price.final_offer.toString());
                //        //console.log(prod_price.final_discount.toString());
                //        special_price = 1;

                //    }
                //}

                for (k = 0; k < cat_discount.length; k++) // for each of deal cats
                {
                    if (arr_cat.indexOf(cat_discount[k].id) > -1) {
                        prod_price.final_offer = prod_price.final_offer * (1 - cat_discount[k].extra_off);
                        special_price = 1;
                    }
                }
                /* for (k = 0; k < sku_discount.length; k++) // for each of deal cats
                {
                    if (prod.sku == sku_discount[k].id) {
                        prod_price.final_offer = prod.price_final_offer * (1 - parseFloat(sku_discount[k].extra_off).toFixed(3));
                        special_price = 1;
                    }
                }*/
                for (k = 0; k < sku_discount.length; k++) // for each of deal cats
                {
                    if (prod.sku == sku_discount[k].id) {
                        prod_price.final_offer = prod_price.final_offer * (1 - sku_discount[k].extra_off);
                        special_price = 1;
                    }
                }


                prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));


                /**************Added By To Calculate Special Discount For Brand***********/
                for (k = 0; k < brand_discount.length; k++) // for each of deal cats
                {
                    if (prod.brand == brand_discount[k].brand) {
                        prod_price.final_offer = prod_price.final_offer * (1 - brand_discount[k].extra_off);
                        special_price = 1;
                    }
                }

                prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));

                /**********************************************************/



                prod_price.final_offer = prod_price.final_offer.toFixed(0);
                prod_price.final_discount = (1 - (prod_price.final_offer / prod_price.mrp)) * 100;
                prod_price.final_discount = prod_price.final_discount.toFixed(2);//Diwakar
                prod.point = prod_price.final_offer * conv_ratio;
                prod.point = prod.point.toFixed(0);
                prod.price = prod_price;
                //productList.push(prod);

                var excluded = false;
                //console.log(prod.brand.toLowerCase())   ;
                for (j = 0; j < brand_exclusion.length; j++) {
                    if (prod.brand.toLowerCase() === brand_exclusion[j].toLowerCase()) {
                        console.log(prod.brand.toLowerCase());
                        excluded = true;
                        break;
                    }
                }
                if (excluded === false) {
                    productList.push(prod);
                }


            }
        }

        nextService.nextList = productList;

        //alert (productList[0].price.list);
        if (productList.length > 0) {
            $scope.productList = productList;
            $scope.productList_original = productList;
            $scope.special_price = special_price;
        }
    }
    var get_brands = function () {
        var brands_filter, tmp = [];
        for (i = 0; i < $scope.productList.length; i++) {
            var excluded = false;
            var prod_brand = $scope.productList[i].brand;
            console.log(prod_brand.toLowerCase());
            for (j = 0; j < brand_exclusion.length; j++) {
                if (prod_brand.toLowerCase() === brand_exclusion[j].toLowerCase()) {
                    excluded = true;
                    break;
                }
            }
            if (excluded === false) {
                tmp.push(
                    prod_brand.toUpperCase()
                )
            }

        }
        $scope.brand_filter = _.uniq(tmp);
    }

    var filters = [];
    $scope.apply_filter = function (filter_name, filter_value) {
        var found = false;
        for (i = 0; i < filters.length; i++) {
            if (filters[i].name == filter_name) {
                if (filters[i].value == filter_value) {
                    filters.splice(i, 1);
                    found = true;
                }
            }
        }

        if (!found) {
            filters.push({ name: filter_name, value: filter_value });
        }
        var brand_filter = _.where(filters, { name: "brand" });
        var feature_filter = _.difference(filters, brand_filter);// .without(filters, {name:"brand"});
        // apply filter on original productlist and copy to display one
        //console.log(filters);
        var prodlist_tmp = $scope.productList_original;

        if (filters.length != 0) {
            var l_found;
            var l_brand_found;

            var prodList_filtered = [];
            var prod_list_feature_filtered = [];
            console.log(prodlist_tmp);
            // first filter for features
            for (i = 0; i < prodlist_tmp.length; i++) {
                var prod = prodlist_tmp[i];
                var prod_features = prodlist_tmp[i].feature;

                l_found = false;
                if (feature_filter.length > 0) {
                    for (j = 0; j < feature_filter.length ; j++) {
                        for (k = 0; k < prod_features.length; k++) {
                            if (prod_features[k].name.toLowerCase() == _.str.trim(feature_filter[j].name.toString()).toLowerCase()) {
                                if (prod_features[k].values.toLowerCase() == _.str.trim(feature_filter[j].value.toString()).toLowerCase()) {
                                    l_found = true;
                                    console.log("break");
                                    break;
                                }
                            }
                        }
                    }
                    if (l_found) {
                        prod_list_feature_filtered.push(prod);
                    }
                }
                else {
                    prod_list_feature_filtered = prodlist_tmp;
                }
            }
            // now filter for brand
            if (brand_filter.length > 0) {
                for (i = 0; i < prod_list_feature_filtered.length; i++) {
                    l_brand_found = false;
                    var prod_tmp = prod_list_feature_filtered[i];
                    for (p = 0; p < brand_filter.length ; p++) {
                        if (prod_tmp.brand.toLowerCase() == brand_filter[p].value.toLowerCase()) {
                            l_brand_found = true;
                            break;
                        }
                    }
                    if (l_brand_found) {
                        prodList_filtered.push(prod_tmp);
                    }

                }
            }
            else {
                prodList_filtered = prod_list_feature_filtered;
            }
            //console.log(prodList_filtered);
            // *********************
            $scope.productList = prodList_filtered;

        }
        else {
            $scope.productList = $scope.productList_original;
        }

    }

    $scope.addTocart = function (productDetails) {
        if (productDetails.have_child == 1) {
            state_managerService.refer_url = $location.path();
            var url = "/quickbuy/" + productDetails.id + "/1";
            $location.path(url);
        }
        else {
            $timeout(function () {
                cartService.addTocart(productDetails);
                //$scope.cart_size = cartService.get_Cart_Size();
                $rootScope.$broadcast('cart_changed');
            }, 500);
        }
    }

    $scope.openQuickBuy = function (product_id) {
        state_managerService.refer_url = $location.path();
        $location.path('/quickbuy/' + product_id);
    }


});

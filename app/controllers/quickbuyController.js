/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 14/9/13
 * Time: 7:51 AM
 * To change this template use File | Settings | File Templates.
 */

app.controller('quickbuyController', function (
                                $scope, $timeout, $rootScope,
                                $http,$q,  $location, $routeParams,
                                productService,catService, cartService,
                                utilService, state_managerService, localStorageService ) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    var product_id =$routeParams.prodId  ;
    var cart_ind = $routeParams.cart;
    if (cart_ind == 1){
        $scope.cart_ind = true;
    }
    else {
        $scope.cart_ind = false;
    }
    $scope.product_id = product_id;
    init();
    function check_login (){
        if (localStorageService.get('user_info') == null){
            $location.path("/login");
        }
    }
    function init() {
        check_login();
        var product_details, parent_product, child_product;
        child_product = [];
        productService.get_productDetailsById($http, $q, product_id).then(function(data){
                product_details = data;
                parent_product = product_details[0];
                $scope.selected_image =  JSON.parse(parent_product).image_urls[0].link;
                $scope.selected_image_zoom =  JSON.parse(parent_product).image_urls[0].zoom_link;
                if (product_details.length > 1){
                    $scope.size_ind = true;
                    for (i=1; i< product_details.length; i++){
                        if (JSON.parse(product_details[i]).stock  >0 ){
                            child_product.push(product_details[i])
                        }
                    }
                    if (child_product.length > 0){
                        $scope.out_of_stock = 0;
                        $scope.stock_msg = "In stock";
                    }
                    else{
                        $scope.out_of_stock = 1;
                        $scope.stock_msg = "Out of stock";
                    }
                }
                else {
                    $scope.size_ind = false;
                    var stock = JSON.parse(parent_product).stock;
                    //console.log(stock);
                    if (stock <= 0){
                        $scope.out_of_stock = 1;
                        $scope.stock_msg = "Out of stock";
                    }
                    else{
                        if ((stock <= 10 ) && (stock > 0)){
                            $scope.out_of_stock = 0;
                            $scope.stock_msg = "Only " + stock.toString() + " left";
                        }
                        else {
                            $scope.out_of_stock = 0;
                            $scope.stock_msg = "In stock";
                        }
                    }


                    //console.log(JSON.parse(parent_product));
                }
                process_price_logic(JSON.parse(parent_product));
                var opt = [];


                for (i=0; i<child_product.length; i++){
                    opt.push(JSON.parse(child_product[i]))   ;
                }
                $scope.myOptions =opt;
                $scope.myModel = ""  ;
                $scope.brand_url = "#/brand/" + JSON.parse(parent_product).brand.toLowerCase();
                $scope.brand_logo = s3_brand_logo_url + JSON.parse(parent_product).brand.toLowerCase() + ".jpg";
            },
            function(){
                //Display an error message
                $scope.error= error;
            });
        $scope.$watch('myModel', function(v){
            $scope.selected_prod = v;
            //alert (v);
        });
        if (cart_ind == 1){
            $scope.quick_buy_button_text = "Add to Cart";
        }
        else{
            $scope.quick_buy_button_text = "Quick Buy";
        }



    }

    var process_price_logic = function (prod){
        var productList = [];
        var conv_ratio = utilService.get_conversion_ratio();
        var store_type =  utilService.get_store_type();
        var customer_discount =  utilService.get_customer_discount();
        var cat_discount =  utilService.get_cat_discount();
        var sku_discount = utilService.get_sku_discount();

        var brand_discount = utilService.get_brand_discount();

        var arr_cat, arr_sku, special_price;
        special_price = 0;
        if (store_type !=  "R") // store is not Retail Only
        {
            //prod_price = JSON.parse(prod.price);
            if ((typeof prod.price) =="object"){
                prod_price = prod.price;
            }
            else {
                prod_price =  JSON.parse(prod.price.toString());
            }
            if (customer_discount != ""  ){
                prod_price.final_offer = prod_price.final_offer * (1-parseFloat(customer_discount).toFixed(2));
                special_price = 1;
            }
            arr_cat =  prod.cat_id; // cats this product belongs to
            //for(k=0; k<cat_discount.length; k++) // for each of deal cats
            //{
            //    if (arr_cat.indexOf(cat_discount[k].id) > -1){
            //        prod_price.final_offer = prod_price.final_offer * (1-parseFloat(cat_discount[k].extra_off).toFixed(2));
            //        special_price = 1;
            //    }
            //}
            //for(k=0; k<sku_discount.length; k++) // for each of deal cats
            //{
            //    if (prod.sku == sku_discount[k].id )
            //    {
            //        prod_price.final_offer = prod_price.final_offer * (1-parseFloat(sku_discount[k].extra_off).toFixed(2));
            //        special_price = 1;
            //    }
            //}

            for (k = 0; k < cat_discount.length; k++) // for each of deal cats
            {
                if (arr_cat.indexOf(cat_discount[k].id) > -1) {
                    prod_price.final_offer = prod_price.final_offer * (1 - cat_discount[k].extra_off);
                    special_price = 1;
                }
            }
            /* for (k = 0; k < sku_discount.length; k++) // for each of deal cats
            {
                if (prod.sku == sku_discount[k].id) {
                    prod_price.final_offer = prod.price_final_offer * (1 - parseFloat(sku_discount[k].extra_off).toFixed(3));
                    special_price = 1;
                }
            }*/
            for (k = 0; k < sku_discount.length; k++) // for each of deal cats
            {
                if (prod.sku == sku_discount[k].id) {
                    prod_price.final_offer = prod_price.final_offer * (1 - sku_discount[k].extra_off);
                    special_price = 1;
                }
            }


            prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));


            /**************Added By To Calculate Special Discount For Brand***********/
            for (k = 0; k < brand_discount.length; k++) // for each of deal cats
            {
                if (prod.brand == brand_discount[k].brand) {
                    prod_price.final_offer = prod_price.final_offer * (1 - brand_discount[k].extra_off);
                    special_price = 1;
                }
            }

            prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));

            /**********************************************************/




            prod_price.final_offer = prod_price.final_offer.toFixed(0);
            prod_price.final_discount =(1-(prod_price.final_offer / prod_price.mrp)) * 100 ;
            prod_price.final_discount =prod_price.final_discount.toFixed(2);
            prod.point =prod_price.final_offer * conv_ratio;
            prod.point = prod.point.toFixed(0);
            $scope.productDetails = prod;
            //$scope.product_price = JSON.parse($scope.productDetails.price);
            $scope.product_price = prod_price;
        }

        $scope.selected_image =  $scope.productDetails.image_urls[0].link;
    }


    var get_bread_crumb = function (cat_id){
        if (cat_id != null){
            catService.get_breadcrumb($http, $q, cat_id).then(function(data){
                    //Update UI using data or use the data to call another service
                    cats = data;
                    $scope.cats = [];
                    for (i=0; i< cats.length; i++){
                        $scope.cats.push( JSON.parse(cats[i].toString()));
                    }
                },
                function(){
                    //Display an error message
                    $scope.error= error;
                });
        }
    }

    $scope.addTocart =  function (productDetails){
        //cartService.addTocart(productDetails.id, 1, productDetails.price.mrp, productDetails.price.mrp);
        if (($scope.myOptions != "") && ($scope.selected_prod == "") ){
            // this is a condition when we have sizes but it's not selected
            alert ("Please select size !");
        }
        else {
            $timeout(function() {
                cartService.addTocart(productDetails, $scope.selected_prod, $scope.myOptions);
                $rootScope.$broadcast('cart_changed');
                if (cart_ind == 1){
                    $location.path(state_managerService.refer_url);
                }
                else
                {
                    $location.path("/checkout");
                }
            }, 500);

        }

    }

    $scope.changeImage=  function (img, $element){
        $scope.selected_image =img.link;
    }

    $scope.close_quick_buy = function(){
        $location.path(state_managerService.refer_url);
    }


    $scope.addtowishlist = function (productDetails) {
        //cartService.addTocart(productDetails.id, 1, productDetails.price.mrp, productDetails.price.mrp);

        if (($scope.myOptions != "") && ($scope.selected_prod == "")) {
            // this is a condition when we have sizes but it's not selected
            alert("Please select size !");
        }
        else {
            $timeout(function () {
                cartService.addTowishlist(productDetails, $scope.selected_prod, $scope.myOptions);
                $rootScope.$broadcast('wishlist_changed');
            }, 1000);

        }

    }



});

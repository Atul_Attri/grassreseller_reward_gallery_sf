/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 24/6/13
 * Time: 9:26 PM
 * To change this template use File | Settings | File Templates.
 */
//This controller retrieves data from the cache/cat.json and associates it with the $scope
//The $scope is ultimately bound to the menu view
app.controller('CatController', function (
                            $scope, $rootScope, $timeout,
                            $location, $routeParams, $http,
                            $q, catService, utilService,
                            cartService, state_managerService, nextService,
                            localStorageService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    var cat_id = $routeParams.catId;
    init();
    $scope.sort_defn = "Sort By ..."
    $scope.$watch('sortorder', function () {
        switch ($scope.sortorder) {
            case "Name":
                $scope.sort_defn = "Name";
                break;
            case "+price.final_offer":
                $scope.sort_defn = "Price (Low to High)";
                break;
            case "-price.final_offer":
                $scope.sort_defn = "Price (High to Low)";
                break;
            case "-price.discount":
                $scope.sort_defn = "Overall Discount (high to low)";
                break;
            case "+brand":
                $scope.sort_defn = "Brand";
                break;
            default:
                $scope.sort_defn = "Sort By ...";
                break;
        }
    })

    function set_filter(cat_id) {
        // get data
        //alert (cat_id);
        var cat_details, selected_cat;// = catService.get_cat_details(cat_id, $http, $q);
        catService.get_cat($http, $q, cat_id).then(function (data) {
            //Update UI using data or use the data to call another service
            cat_details = data;
            if (cat_details.length != 0) {
                //console.log(data);
                selected_cat = JSON.parse(cat_details[0].toString());
                $scope.cat_name = selected_cat.Name;
                $scope.filters = selected_cat.filters;
                //console.log($scope.filters);
                $scope.checked_vals = [selected_cat.filters[0]];
            }
            else {
                //selected_cat =  JSON.parse(cat_details[0].toString());
                $scope.cat_name = "";
                $scope.filters = [];
                $scope.checked_vals = [selected_cat.filters[0]];
            }
        },
            function () {
                //Display an error message
                $scope.error = error;
            });

        catService.get_cat_details($http, $q, cat_id).then(function (data) {
            if (data.length != 0) {
                if (JSON.parse(data[0].toString()).image_urls.length > 0) {
                    $scope.cat_image = JSON.parse(data[0].toString()).image_urls[0].link;
                }
                else {
                    $scope.cat_image = "";
                }
            }
            else {
                $scope.cat_image = "";
            }

        },

        function () {
            //Display an error message
            $scope.error = error;
        });

        catService.get_breadcrumb($http, $q, cat_id).then(function (data) {
            //Update UI using data or use the data to call another service
            var id, tmp, level;
            cats = data;
            $scope.cats = [];
            // hardcoded home node
            var home = '{"id": "0","Name": "Home","description": "Go to Home", "level": 0}';
            $scope.cats.push(JSON.parse(home));
            for (i = 0; i < cats.length; i++) {
                $scope.cats.push(JSON.parse(cats[i].toString()));
            }
            for (i = 0; i < $scope.cats.length; i++) {
                id = $scope.cats[i].id;
                var tmp = id.split(".");
                if (tmp.length <= 2) {
                    // level 1
                    level = 1
                }
                else {
                    if (tmp[2] == "0") {
                        level = 2;
                    }
                    else {
                        level = 3;
                    }
                }
                $scope.cats[i].level = level;
            }
            //                console.log($scope.cats);
        },
            function () {
                //Display an error message
                $scope.error = error;
            });
        $scope.checked_vals = ""


        //        $scope.$watch('checked_vals', function(v){
        //            console.log('changed', v);
        //        });
    }
    $scope.refresh_filters = function () {
        //console.log($scope.checked_vals ) ;
        //$scope.checked_vals = [];
        set_filter(cat_id);

    }

    //    function check_login (){
    //        if (localStorageService.get('user_info') == null){
    //            //$location.path("/login");
    //            //$window.location.href = root_url + "/index.html#/login";
    //            $window.location.href = root_url + "/login/login.html#/login";
    //        }
    //        else{
    //            user_info =   localStorageService.get('user_info');
    //        }
    //    }
    var user_info;
    var brand_exclusion = utilService.get_brand_exclusion();
    function init() {
        // check user as this is loaded in all pages
        //check_login();
        user_info = localStorageService.get('user_info');
        var lastChar = cat_id[cat_id.length - 1]; // => "1"
        //console.log (cat_id.split(".").length)     ;
        if ((lastChar == "0") && (cat_id.split(".").length == 2)) { // top level menu
            $location.path("/cat1/" + cat_id);
        }
        $scope.cat_id = cat_id;
        $scope.sortorder = 'Name';
        set_filter(cat_id);
        var prod_list;

        catService.get_prod_by_cat($http, $q, cat_id, user_info.min_point_range, user_info.max_point_range).then(function (data) {
            //Update UI using data or use the data to call another service
            prod_list = data;
            //console.log(prod_list);
            process_price_logic(prod_list);
            get_brands();
            //get_brands(prod_list);
            //console.log($scope.brands_filter)  ;
        },

            function () {
                //Display an error message
                $scope.error = error;
            });
        // search brands within searched prod list


        //catService.get_prod_all_info_by_cat($http, $q, cat_id, user_info.min_point_range, user_info.max_point_range).then(function (data) {
        //    //Update UI using data or use the data to call another service
        //    prod_list = data;
        //    process_price_logic(prod_list);
        //    get_brands();

        //},

        //    function () {
        //        //Display an error message
        //        $scope.error = error;
        //    });




    }
    var process_price_logic = function (prod_list) {
        var productList = [];
        var conv_ratio = utilService.get_conversion_ratio();
        var store_type = utilService.get_store_type();

        var filter_display = utilService.get_filter_display(); //Added By Hassan Ahamed 

        var customer_discount = utilService.get_customer_discount();
        var cat_discount = utilService.get_cat_discount();
        var sku_discount = utilService.get_sku_discount();

        var brand_discount = utilService.get_brand_discount();

        //console.log(cat_discount);
        var arr_cat, arr_sku, special_price;
        special_price = 0;

        $scope.store_type = store_type;
        $scope.filter_display = filter_display; //Added By Hassan Ahamed Start


        //        if (store_type == "P"){
        //            $scope.display_currency = 0;
        //        }
        //        if (store_type !=  "R") // store is not Retail Only
        //        {
        for (i = 0; i < prod_list.length; i++) {
            prod = JSON.parse(prod_list[i].toString());

            if ((typeof prod.price) == "object") {
                prod_price = prod.price;
            }
            else {
                prod_price = JSON.parse(prod.price.toString());
            }

            if (customer_discount != "") {
                prod_price.final_offer = prod_price.final_offer * (1 - parseFloat(customer_discount).toFixed(2));
                special_price = 1;
                //console.log(prod.price.final_offer);
            }
            arr_cat = prod.cat_id; // cats this product belongs to
            for (k = 0; k < cat_discount.length; k++) // for each of deal cats
            {
                if (arr_cat.indexOf(cat_discount[k].id) > -1) {
                    prod_price.final_offer = prod_price.final_offer * (1 - cat_discount[k].extra_off);
                    special_price = 1;
                }
            }
            /* for (k = 0; k < sku_discount.length; k++) // for each of deal cats
            {
                if (prod.sku == sku_discount[k].id) {
                    prod_price.final_offer = prod.price_final_offer * (1 - parseFloat(sku_discount[k].extra_off).toFixed(3));
                    special_price = 1;
                }
            }*/
            for (k = 0; k < sku_discount.length; k++) // for each of deal cats
            {
                if (prod.sku == sku_discount[k].id) {
                    prod_price.final_offer = prod_price.final_offer * (1 - sku_discount[k].extra_off);
                    special_price = 1;
                }
            }


            prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));


            /**************Added By To Calculate Special Discount For Brand***********/
            for (k = 0; k < brand_discount.length; k++) // for each of deal cats
            {
                if (prod.brand == brand_discount[k].brand) {
                    prod_price.final_offer = prod_price.final_offer * (1 - brand_discount[k].extra_off);
                    special_price = 1;
                }
            }

            prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));

            /**********************************************************/


            prod_price.final_discount = (1 - (prod_price.final_offer / prod_price.mrp)) * 100;
            //prod_price.final_discount = parseInt(prod_price.final_discount.toFixed(0));//commented for enabling actual %age bug id :236.. Diwakar 21/01/2014
            prod_price.final_discount = prod_price.final_discount.toFixed(2);
            prod.point = prod_price.final_offer * conv_ratio;
            prod.point = prod.point.toFixed(0);

            prod.price = prod_price;

            var excluded = false;
            if ((prod.stock < 0) && (prod.stock != -9999)) {
                excluded = true;
            }
            if (brand_exclusion.length > 0) {
                for (j = 0; j < brand_exclusion.length; j++) {
                    if (prod.brand.toLowerCase() === brand_exclusion[j].toLowerCase()) {
                        excluded = true;
                        break;
                    }
                }
            }
            if (excluded === false) {
                productList.push(prod);
            }
            //console.log(prod);
        }


        nextService.nextList = productList;

        if (filter_display == "P") {//Added By Hassan Ahamed Start

            if (productList.length > 0) {
                $scope.productList = productList;
                $scope.productList_original = productList;
                $scope.special_price = special_price;
                //console.log(productList);
                $scope.max_price_sku = _.max($scope.productList_original,
                    function (p) {
                        return parseInt(p.point);
                    }).point;
                $scope.min_price_sku = _.min($scope.productList_original,
                        function (p) {
                            return parseInt(p.point);
                        }).point;
            }
        }

        else {//Added By Hassan Ahamed End

            if (productList.length > 0) {
                $scope.productList = productList;
                $scope.productList_original = productList;
                $scope.special_price = special_price;
                //console.log(productList);
                $scope.max_price_sku = _.max($scope.productList_original,
                    function (p) {
                        return parseInt(p.price.final_offer);
                    }).price.final_offer;
                $scope.min_price_sku = _.min($scope.productList_original,
                        function (p) {
                            return parseInt(p.price.final_offer);
                        }).price.final_offer;
            }
        }
    }
    //var get_brands = function (prod_list){
    var get_brands = function () {
        var brands_filter, tmp = [];
        for (i = 0; i < $scope.productList.length; i++) {
            var excluded = false;
            var prod_brand = $scope.productList[i].brand;
            //console.log(prod_brand.toLowerCase())   ;
            console.log(brand_exclusion);
            if (brand_exclusion.length > 0) {
                for (j = 0; j < brand_exclusion.length; j++) {
                    if (prod_brand.toLowerCase() === brand_exclusion[j].toLowerCase()) {
                        excluded = true;
                        break;
                    }
                }
            }
            if (excluded === false) {
                tmp.push(
                    prod_brand.toUpperCase()
                    )
            }

        }
        $scope.brand_filter = _.uniq(tmp);
    }

    var filters = [];

    $scope.apply_filter = function (filter_name, filter_value) {
        var found = false;
        for (i = 0; i < filters.length; i++) {
            if (filters[i].name == filter_name) {
                if (filters[i].value == filter_value) {
                    // special treatment for discount and price filters
                    if ((filter_name != "discount") && (filter_name != "price")) {
                        filters.splice(i, 1);
                        found = true;
                    }
                }
            }
        }

        if (!found) {
            filters.push({ name: filter_name, value: filter_value });
        }
        if (filter_name == "price") {
            var filter_price = true;
        }
        if (filter_name == "discount") {
            var filter_discount = true;
        }

        var brand_filter = _.where(filters, { name: "brand" });
        var price_filter = _.where(filters, { name: "price" });
        var discount_filter = _.where(filters, { name: "discount" });
        var feature_filter = _.difference(filters, brand_filter, price_filter, discount_filter);// .without(filters, {name:"brand"});


        // apply filter on original productlist and copy to display one
        var prodlist_tmp = $scope.productList_original;
        //console.log     (filters.length );
        if (filters.length != 0) {
            var l_found;
            var l_brand_found;

            var prodList_filtered = [];
            var prod_list_feature_filtered = [];
            var prod_list_price_filtered = [];
            var prod_list_discount_filtered = [];


            //console.log($scope.discount_filter);
            // first filter for features
            if (feature_filter.length > 0) {
                for (i = 0; i < prodlist_tmp.length; i++) {
                    var prod = prodlist_tmp[i];
                    var prod_features = prodlist_tmp[i].feature;
                    l_found = false;
                    if (feature_filter.length > 0) {
                        for (j = 0; j < feature_filter.length ; j++) {
                            for (k = 0; k < prod_features.length; k++) {
                                if (prod_features[k].name.toLowerCase() == feature_filter[j].name.toLowerCase()) {
                                    //l_found = false;
                                    if ((prod_features[k].values != null) && (feature_filter[j].value != null)) {
                                        if (prod_features[k].values.toLowerCase() == feature_filter[j].value.toLowerCase()) {
                                            l_found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if (l_found) {
                            prod_list_feature_filtered.push(prod);
                        }
                    }
                    else {
                        prod_list_feature_filtered = prodlist_tmp;
                    }
                }
            }
            else {
                prod_list_feature_filtered = prodlist_tmp;
            }
            // now filter for brand
            if (brand_filter.length > 0) {
                for (i = 0; i < prod_list_feature_filtered.length; i++) {
                    l_brand_found = false;
                    var prod_tmp = prod_list_feature_filtered[i];
                    for (p = 0; p < brand_filter.length ; p++) {
                        if (prod_tmp.brand.toLowerCase() == brand_filter[p].value.toLowerCase()) {
                            l_brand_found = true;
                            break;
                        }
                    }
                    if (l_brand_found) {
                        prodList_filtered.push(prod_tmp);
                    }

                }
            }
            else {
                prodList_filtered = prod_list_feature_filtered;
            }
            //console.log(prodList_filtered);
            // now filter for pricerange apply that on prodList_filtered
            if (filter_price) {

                for (i = 0; i < prodList_filtered.length; i++) {

                    var prod_tmp = prodList_filtered[i];



                    //console.log(prod_tmp);

                    if (filter_display == "P") { //Added By Hassan Ahamed Start

                        if (
                            (parseInt(prod_tmp.point) >= $scope.min_price_sku)
                            &&
                            (parseInt(prod_tmp.point) <= $scope.max_price_sku)
                            ) {
                            // push to new prodlist
                            prod_list_price_filtered.push(prod_tmp);
                        }

                    }
                    else {//Added By Hassan Ahamed End

                        if (
                            (parseInt(prod_tmp.price.final_offer) >= $scope.min_price_sku)
                            &&
                            (parseInt(prod_tmp.price.final_offer) <= $scope.max_price_sku)
                            ) {
                            // push to new prodlist
                            prod_list_price_filtered.push(prod_tmp);
                        }
                    }


                }
                prodList_filtered = prod_list_price_filtered;
            }
            //console.log(prodList_filtered);
            // now filter for discount apply that on prodList_filtered

            if (filter_discount) {


                for (i = 0; i < prodList_filtered.length; i++) {
                    var prod_tmp = prodList_filtered[i];
                    //console.log(prod_tmp);

                    switch (parseInt($scope.discount_filter)) {
                        case 10:
                            // 0 - 10
                            //console.log("0-10");
                            if ((parseInt(prod_tmp.price.final_discount) >= 0)
                                &&
                                (parseInt(prod_tmp.price.final_discount) <= 10)) {
                                prod_list_discount_filtered.push(prod_tmp);
                            }

                            break;
                        case 25:
                            // 10-25
                            //console.log("10-25");
                            if ((parseInt(prod_tmp.price.final_discount) >= 10)
                                &&
                                (parseInt(prod_tmp.price.final_discount) <= 25)) {
                                prod_list_discount_filtered.push(prod_tmp);
                            }
                            break;
                        case 100:
                            // >25
                            if (parseInt(prod_tmp.price.final_discount) >= 25) {
                                prod_list_discount_filtered.push(prod_tmp);
                            }
                            break;
                        default:
                            //console.log("default");
                            prod_list_discount_filtered.push(prod_tmp);
                            break;
                    }
                }
                prodList_filtered = prod_list_discount_filtered
            }

            // *********************
            $scope.productList = prodList_filtered;

        }
        else {
            $scope.productList = $scope.productList_original;
        }

    }

    $scope.addTocart = function (productDetails) {


        if (productDetails.have_child == 1) {
            state_managerService.refer_url = $location.path();
            var url = "/quickbuy/" + productDetails.id + "/1";
            $location.path(url);
        }
        else {
            $timeout(function () {
                cartService.addTocart(productDetails);
                //$scope.cart_size = cartService.get_Cart_Size();
                $rootScope.$broadcast('cart_changed');
            }, 500);

        }

    }

    $scope.openQuickBuy = function (product_id) {
        state_managerService.refer_url = $location.path();
        $location.path('/quickbuy/' + product_id);
    }


});

app.filter('sort', function () {
    return function (items) {
        return _.sortBy(items, function (val) {
            return val;
        });
    };


    //    _.sortBy(items, function(val){
    //        return val;
    //    }); // [1, 2, 3]
    //    return function(items) {
    //        return items.slice().reverse();
    //    };
});
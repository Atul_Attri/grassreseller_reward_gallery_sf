/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 28/6/13
 * Time: 9:04 PM
 * To change this template use File | Settings | File Templates.
 */

app.controller('ProductDetailsController', function (
    $scope, $timeout, $rootScope, $http, $q, $location,
    $routeParams, productService, catService, cartService, 
    nextService, utilService, localStorageService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below


    var product_id = $routeParams.prodId;
    $scope.product_id = product_id;


    //localStorageService.set('first_product_index', "0");

    var ProdList = [];
    ProdList = nextService.nextList;

    if (typeof ProdList === 'undefined') {
        $scope.MultipleItems = 0;
    }
    else {
        $scope.AllProductList = ProdList;
        $scope.MultipleItems = ProdList.length;
        var Prod_Id_Url = "";

        for (var i = 0; i < ProdList.length; i++) {
            if (product_id == ProdList[i].id) {
                //if (localStorageService.get('first_product_index') != "0") {
                //    localStorageService.set('first_product_index', i.toString());
                //}
                if ((i + 1) == ProdList.length) {
                    Prod_Id_Url = ProdList[0].id;
                }
                else {
                    Prod_Id_Url = ProdList[i + 1].id;
                }
                $scope.prod_id = Prod_Id_Url;
            }
        }

        //var PodFrstIndex = localStorageService.get('first_product_index') * 1;
        //alert(PodFrstIndex);
        //PodFrstIndex = PodFrstIndex.toFixed(0);

        //for (m = PodFrstIndex; m < (ProdList.length + PodFrstIndex) ; m++) {
        //    var j = m > ProdList.length ? (m - ProdList.length) : m;
        //    Prod_Id_Url = ProdList[j].id;
        //    $scope.prod_id = Prod_Id_Url;
        //}

    }


    var prod_view = {
        user_id: "",
        id: "",
        sku: "",
        brand: "",
        Name: "",
        price: "",
        Express: ""
    }
    init();
    function check_login() {
        if (localStorageService.get('user_info') == null) {
            $location.path("/login");
        }
    }
    function init() {
        check_login();

        var product_details, parent_product, child_product;
        child_product = [];
        productService.get_productDetailsById($http, $q, product_id).then(function (data) {
            product_details = data;
            parent_product = product_details[0];
            $scope.selected_image = JSON.parse(parent_product).image_urls[0].link;
            $scope.selected_image_zoom = JSON.parse(parent_product).image_urls[0].zoom_link;
            if (product_details.length > 1) {
                $scope.size_ind = true;

                for (i = 1; i < product_details.length; i++) {
                    //console.log(JSON.parse(product_details[i]).stock);
                    if (JSON.parse(product_details[i]).stock > 0) {
                        child_product.push(product_details[i])
                    }
                }
                //console.log (child_product.length);
                if (child_product.length > 0) {
                    $scope.out_of_stock = 0;
                    $scope.stock_msg = "In stock";
                }
                else {
                    $scope.out_of_stock = 1;
                    $scope.stock_msg = "Out of stock";
                }
            }
            else {
                $scope.size_ind = false;
                var stock = JSON.parse(parent_product).stock;
                //console.log(stock);
                if (stock <= 0) {
                    $scope.out_of_stock = 1;
                    $scope.stock_msg = "Out of stock";
                }
                else {
                    if ((stock <= 10) && (stock > 0)) {
                        $scope.out_of_stock = 0;
                        $scope.stock_msg = "Only " + stock.toString() + " left";
                    }
                    else {
                        $scope.out_of_stock = 0;
                        $scope.stock_msg = "In stock";
                    }
                }


                //console.log(JSON.parse(parent_product));
            }

            //console.log($scope.out_of_stock);
            process_price_logic(JSON.parse(parent_product));

            prod_view.user_id = localStorageService.get('user_info').email_id;
            prod_view.price = $scope.product_price;
            prod_view.id = $scope.productDetails.id;
            prod_view.sku = $scope.productDetails.sku;
            prod_view.brand = $scope.productDetails.brand;
            prod_view.Name = $scope.productDetails.Name;

            productService.prod_view_insert($http, $q, prod_view).then(function (data) {
            },
                function () {
                    //Display an error message
                    $scope.error = error;
                });
            var opt = [];
            for (i = 0; i < child_product.length; i++) {
                //sizes.push(JSON.parse(child_product[i]).size)
                if (JSON.parse(child_product[i]).stock > 0) {
                    opt.push(JSON.parse(child_product[i]));
                }
            }
            $scope.myOptions = opt;
            $scope.myModel = "";
            $scope.brand_url = "#/brand/" + JSON.parse(parent_product).brand.toLowerCase();
            $scope.brand_logo = s3_brand_logo_url + JSON.parse(parent_product).brand.toLowerCase() + ".jpg";
            // getting breadcrumb at end intentionally so that it doesn't block others
            get_bread_crumb(JSON.parse(parent_product).parent_cat_id);
        },
            function () {
                //Display an error message
                $scope.error = "Product Data not found";
            });
        $scope.$watch('myModel', function (v) {
            //console.log('changed', v);
            $scope.selected_prod = v;
            //alert (v);
        });





    }

    var process_price_logic = function (prod) {
        var productList = [];
        var conv_ratio = utilService.get_conversion_ratio();
        var store_type = utilService.get_store_type();
        var customer_discount = utilService.get_customer_discount();
        var cat_discount = utilService.get_cat_discount();
        var sku_discount = utilService.get_sku_discount();

        var brand_discount = utilService.get_brand_discount();

        $scope.store_type = store_type;
        var arr_cat, arr_sku, special_price;
        special_price = 0;
        if (store_type != "R") // store is not Retail Only
        {
            //prod_price = JSON.parse(prod.price);
            if ((typeof prod.price) == "object") {
                prod_price = prod.price;
            }
            else {
                prod_price = JSON.parse(prod.price.toString());
            }
            if (customer_discount != "") {
                prod_price.final_offer = prod_price.final_offer * (1 - parseFloat(customer_discount).toFixed(2));
                special_price = 1;
            }
            arr_cat = prod.cat_id; // cats this product belongs to
            //for (k = 0; k < cat_discount.length; k++) // for each of deal cats
            //{
            //    if (arr_cat.indexOf(cat_discount[k].id) > -1) {
            //        prod_price.final_offer = prod_price.final_offer * (1 - parseFloat(cat_discount[k].extra_off).toFixed(2));
            //        special_price = 1;
            //    }
            //}

            for (k = 0; k < cat_discount.length; k++) // for each of deal cats
            {
                if (arr_cat.indexOf(cat_discount[k].id) > -1) {
                    prod_price.final_offer = prod_price.final_offer * (1 - cat_discount[k].extra_off);
                    special_price = 1;
                }
            }


            //for (k = 0; k < sku_discount.length; k++) // for each of deal cats
            //{
            //    if (prod.sku == sku_discount[k].id) {
            //        prod_price.final_offer = prod_price.final_offer * (1 - parseFloat(sku_discount[k].extra_off).toFixed(2));
            //        special_price = 1;
            //    }
            //}

            for (k = 0; k < sku_discount.length; k++) // for each of deal cats
            {
                if (prod.sku == sku_discount[k].id) {
                    prod_price.final_offer = prod_price.final_offer * (1 - sku_discount[k].extra_off);
                    special_price = 1;
                }
            }


            /**************Added By To Calculate Special Discount For Brand***********/
            for (k = 0; k < brand_discount.length; k++) // for each of deal cats
            {
                if (prod.brand == brand_discount[k].brand) {
                    prod_price.final_offer = prod_price.final_offer * (1 - brand_discount[k].extra_off);
                    special_price = 1;
                }
            }

            prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));

            /**********************************************************/

            prod_price.final_offer = prod_price.final_offer.toFixed(0);
            prod_price.final_discount = (1 - (prod_price.final_offer / prod_price.mrp)) * 100;
            prod_price.final_discount = prod_price.final_discount.toFixed(2);//Diwakar
            prod.point = prod_price.final_offer * conv_ratio;
            prod.point = prod.point.toFixed(0);
            $scope.productDetails = prod;
            //$scope.product_price = JSON.parse($scope.productDetails.price);
            $scope.product_price = prod_price;
        }
        //alert (productList[0].price.list);
        //        $scope.productList = productList;
        //        $scope.special_price = special_price;
        //        console.log("Product - ");
        //        console.log($scope.productDetails.image_urls[0].zoom_link);

    }


    var get_bread_crumb = function (cat_id) {
        if (cat_id != null) {
            catService.get_breadcrumb($http, $q, cat_id).then(function (data) {
                //Update UI using data or use the data to call another service
                cats = data;
                $scope.cats = [];
                for (i = 0; i < cats.length; i++) {
                    $scope.cats.push(JSON.parse(cats[i].toString()));
                }
                for (i = 0; i < $scope.cats.length; i++) {
                    id = $scope.cats[i].id;
                    var tmp = id.split(".");
                    if (tmp.length <= 2) {
                        // level 1
                        level = 1
                    }
                    else {
                        if (tmp[2] == "0") {
                            level = 2;
                        }
                        else {
                            level = 3;
                        }
                    }
                    $scope.cats[i].level = level;
                }
            },
                function () {
                    //Display an error message
                    $scope.error = error;
                });
        }
    }

    $scope.addTocart = function (productDetails) {

      
        //cartService.addTocart(productDetails.id, 1, productDetails.price.mrp, productDetails.price.mrp);


        if (($scope.myOptions != "") && ($scope.selected_prod == "")) {
            // this is a condition when we have sizes but it's not selected
            alert("Please select size !");
        }
        else {
            $timeout(function () {
                cartService.addTocart(productDetails, $scope.selected_prod, $scope.myOptions);
                //$scope.cart_size = cartService.get_Cart_Size();
                $rootScope.$broadcast('cart_changed');
            }, 1000);
            //            cartService.addTocart(productDetails, $scope.selected_prod);
            //            $rootScope.$broadcast('cart_changed');
        }
    }

    $scope.changeImage = function (img, $element) {
        $scope.selected_image = img.zoom_link;
        $scope.selected_image_zoom = img.zoom_link;
    }
    //    $scope.changeImage1=  function (img, $element){
    //        alert ("sss");
    //        $scope.selected_image =img.zoom_link;
    //        $scope.selected_image_zoom =  img.zoom_link;
    //    }




    $scope.addtowishlist = function (productDetails) {
        //cartService.addTocart(productDetails.id, 1, productDetails.price.mrp, productDetails.price.mrp);

        if (($scope.myOptions != "") && ($scope.selected_prod == "")) {
            // this is a condition when we have sizes but it's not selected
            alert("Please select size !");
        }
        else {
            $timeout(function () {
                cartService.addTowishlist(productDetails, $scope.selected_prod, $scope.myOptions);
                $rootScope.$broadcast('wishlist_changed');
            }, 1000);

        }

    }


    $scope.nextProduct = function () {
        var url = "/prod/" + Prod_Id_Url;
        $location.path(url);
    }

});

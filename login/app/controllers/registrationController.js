﻿
app.controller('registrationController', function ($scope, $http, $location, $q, registrationService, localStorageService) {

    $scope.isViewLoading = false;

    $scope.addRegistration = function () {


        var first_name = $.trim($("#first_name").val());
        if (first_name == null || first_name == "") {
            alert('Please Enter First Name');
            return false;
        }

        var last_name = $.trim($("#last_name").val());
        if (last_name == null || last_name == "") {
            alert('Please Enter Last Name');
            return false;
        }

        var mobile_number = $.trim($("#mobile_number").val());
        if (mobile_number == null || mobile_number == "") {
            alert('Please Enter Mobile No');
            return false;
        }

        var email_id = $.trim($("#email_id").val());
        if (email_id == null || email_id == "") {
            alert('Please Enter Company E-mail Address');
            return false;
        }

        var password = $.trim($("#password").val());
        if (password == null || password == "") {
            alert('Please Enter Password');
            return false;
        }

        var confrmpwd = $.trim($("#confrmpwd").val());
        if (confrmpwd == null || confrmpwd == "") {
            alert('Please Enter Confirm Password');
            return false;
        }

        if (password != confrmpwd) {
            alert('Both Password Should Be Same');
            return false;
        }


        var domain = store_email_signature;
        var Length = domain.length;
        var email_domain = domain.substring(2, Length);    
        var regemail = $scope.registration.email_id;

        var returnval = '';
        var chk = 0;
        if (domain == '*.*') {
            chk = 1;
        }
        else {
            chk = regemail.indexOf(email_domain);
        }
        //if (regemail.indexOf(email_domain) != -1) {
        if (chk != -1) {
            $scope.isViewLoading = true;
            //returnval = registrationService.UserRegistration($http, $q, $scope.registration);

            
           
            //var Company = '{"company": "' + store + '"}';
            //var Company = { company: store }
            //$scope.registration.push(Company);
            $scope.registration.company = store;

            registrationService.UserRegistration($http, $q, $scope.registration).then(function (data) {
                console.log(data + " -- " + data.toString().toLowerCase() + " -- " + "success");
                $scope.isViewLoading = false;

                localStorageService.set('message', JSON.parse(data.toString()));

                localStorageService.set('reg_user', $scope.registration);

                $location.path("/message");

                //if (JSON.parse(data.toString()) == "Your Registration Has Been Successfully Completed") {
                //    //$location.path("#/registration_success");
                //    alert("Registration is Successfull now!  Please check your email for activation of account.");
                //    localStorageService.set("user_info", "");
                //    $location.path("#/home");
                //}
                //else if (JSON.parse(data.toString()) == "Your Account Activation Is Pending") {

                //    alert(JSON.parse(data.toString()));
                //    localStorageService.set("user_info", "");
                //    $scope.showButton = function () {
                //        return false;
                //    }
                //}
                //else {
                //    alert(JSON.parse(data.toString()));
                //}
            },
                function () {
                    //Display an error message
                    $scope.error = error;
                });

        } else {
            alert('Your company email domain is not registered with us. Please contact customer support.');

            return false;
        }
  
       

    };


   
});

/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/7/13
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
var adItems = {
    "landing_page_main_scroller" : [{
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/grassreseller/2000points.jpg"
    },{
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/grassreseller/3000points.jpg"
    }],
    "landing_page_static_html" : "https://s3-ap-southeast-1.amazonaws.com/annectos/ads/men_static_ad.html",
    "offer_items" : [{
        "id" : "001",
        "offer_text" : "Only till 24th July midnight | Additional 10% off on Nike Shoes only for Intel employees",
        "href" : "#/cat/1.0/"
    }, {
        "id" : "002",
        "offer_text" : "Additional 5% off on Women Hand Bags only for Intel employees",
        "href" : "#/cat/2.0/"
    }],
    "l1_banner" : [{
        "id" : "400",
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/grassreseller/400points.jpg"
    }, {
        "id" : "700",
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/grassreseller/700points.jpg"
    },{
        "id" : "1000",
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/grassreseller/1000points.jpg"
    },{
        "id" : "1500",
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/grassreseller/1500points.jpg"
    }, {
        "id" : "2000",
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/grassreseller/2000points.jpg"
    }, {
        "id" : "3000",
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/grassreseller/3000points.jpg"
    }, {
        "id" : "3500",
        "href" : "#/cat/7.1.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/schneider/3500.jpg"
    },{
        "id" : "4000",
        "href" : "#/cat/8.1.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/schneider/4000.jpg"
    }]
};
